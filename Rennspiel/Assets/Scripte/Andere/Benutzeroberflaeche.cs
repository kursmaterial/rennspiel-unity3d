﻿using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Benutzeroberflaeche : MonoBehaviour
{
    public Text zeitText;

    private List<Transform> alleUIElemente = new List<Transform>();

    private void Start() {
        Transform[] elemente = GetComponentsInChildren<Transform>();
        alleUIElemente = new List<Transform>();
        alleUIElemente.AddRange(elemente);
        MacheUnsichtbar();
    }

    public void VerlasseSpiel() {
#if UNITY_STANDALONE
        Application.Quit();
#endif

#if UNITY_EDITOR
        UnityEditor.EditorApplication.isPlaying = false;
#endif
    }

    public void ButtonVerhaltenNochmal() {
        Spielmanager.Instanz.StarteSpielNeu();
    }

    public void SetzeZeit(float zeit) {
        zeit = (float) Math.Round(zeit, 2);
        zeitText.text = zeit.ToString();
    }

    public void MacheSichtbar () {
        for(int i = 0; i < alleUIElemente.Count; i++) {
            if (alleUIElemente[i].gameObject != gameObject) {
                alleUIElemente[i].gameObject.SetActive(true);
            }
        }
    }

    public void MacheUnsichtbar () {
        for (int i = 0; i < alleUIElemente.Count; i++) {
            if (alleUIElemente[i].gameObject != gameObject) {
                alleUIElemente[i].gameObject.SetActive(false);
            }
        }
    }
}
