﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VerfolgendeKamera : MonoBehaviour
{
    [Range(0, 1)]
    public float geschwindigkeit = .3f;

    [Range(0, 1)]
    public float drehungInKurve = .1f;

    public Vector3 sollVersatz;
    public AutoSteuerung auto;

    [HideInInspector]
    public bool kannSichBewegen = true;

    void FixedUpdate()
    {
        if(kannSichBewegen)
            transform.position = Vector3.Lerp(transform.position, SollPosition(), geschwindigkeit);
        transform.LookAt(Kameraziel());
    }

    private Vector3 SollPosition () {
        Vector3 ohneRotation = SollPositionOhneRotation();
        return ohneRotation + (auto.werteUndZustaende.rb.transform.right * sollVersatz.z) * auto.werteUndZustaende.lenkeinschlag * Mathf.Clamp01(drehungInKurve);
    }

    private Vector3 SollPositionOhneRotation () {
        Vector3 position = auto.werteUndZustaende.rb.position;

        position += Vector3.ProjectOnPlane(auto.werteUndZustaende.rb.transform.forward, Vector3.up).normalized * sollVersatz.z;
        position += Vector3.ProjectOnPlane(auto.werteUndZustaende.rb.transform.right, Vector3.up) * sollVersatz.x;
        position.y += sollVersatz.y;

        return position;
    }

    private Vector3 Kameraziel () {
        return auto.werteUndZustaende.rb.position + Vector3.ProjectOnPlane(auto.transform.forward, Vector3.up) * Mathf.Clamp(auto.werteUndZustaende.GeschwindigkeitForwaerts() * .1f, -1, 20);
    }
}
