﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spielmanager : MonoBehaviour
{
    #region Sigleton Pattern
    private static Spielmanager instanz = null;
    private static readonly Object threadLock = new Object();

    public static Spielmanager Instanz {
        get {
            lock (threadLock) {
                if (instanz != null)
                    return instanz;

                Spielmanager[] instanzen = FindObjectsOfType<Spielmanager>();
                int anzahlInstanzen = instanzen.Length;

                if (anzahlInstanzen == 0)
                    return instanz = Instantiate(new GameObject()).AddComponent<Spielmanager>();

                for (int i = 1; i < anzahlInstanzen; i++)
                    Destroy(instanzen[i].gameObject);

                return instanz = instanzen[0];
            }
        }
    }

    private void Awake() {
        if (Instanz == this) {
            DontDestroyOnLoad(this.gameObject);
        }
    }
    #endregion

    private List<AutoSteuerung> autos = new List<AutoSteuerung>();
    private List<AutoSteuerung> Autos {
        get {
            if (autos.Count == 0) {
                AutoSteuerung[] alleAutos = FindObjectsOfType<AutoSteuerung>();

                if (alleAutos.Length == 0)
                    Debug.LogWarning("<color=#0075cf>Es gibt kein Auto im Level. Wenn wir fahren wollen wäre das aber schon nötig.</color>");

                if (alleAutos.Length > 4)
                    Debug.LogWarning("<color=#0075cf>" + alleAutos.Length + " sind ein paar viele Autos. Du solltest maximal 4 im Level haben.</color>");

                autos.Clear();
                for(int i = 0; i < 4 && i < alleAutos.Length; i++)
                    autos.Add(alleAutos[i]);
            }
            return autos;
        }
        set {
            autos = value;
        }
    }

    private Startpunkt start;
    private Startpunkt StartpunktHaupt {
        get {
            if (start == null) {
                Startpunkt[] startpunkte = FindObjectsOfType<Startpunkt>();
                int anzahlStartpunkte = startpunkte.Length;

                if (anzahlStartpunkte == 0)
                    return null;

                if (anzahlStartpunkte > 1)
                    Debug.LogWarning("<color=#0075cf>Obacht! Es gibt mehr als einen Start. Dem sollte nicht so sein!</color>");

                start = startpunkte[0];
            }
            return start;
        }
        set {
            start = value;
        }
    }

    private Benutzeroberflaeche oberflaeche;
    private Benutzeroberflaeche Oberflaeche {
        get {
            if (oberflaeche == null) {
                Benutzeroberflaeche[] oberflaechen = FindObjectsOfType<Benutzeroberflaeche>();
                int anzahlOberflaechen = oberflaechen.Length;

                if (anzahlOberflaechen == 0)
                    return null;

                if(anzahlOberflaechen > 1)
                    Debug.LogWarning("<color=#0075cf>Obacht! Es gibt mehr als eine Benutzeroberfläche. Dem sollte nicht so sein! Ein Menü sollte reichen.</color>");

                oberflaeche = oberflaechen[0];
            }
            return oberflaeche;
        }
        set {
            oberflaeche = value;
        }
    }

    private float startzeit = 0;
    private bool rennenLaueft = false;




    private void Start() {
        StarteSpielNeu();
    }

    public void StarteSpielNeu() {
        if(Oberflaeche != null)
            Oberflaeche.MacheUnsichtbar();

        SetCarToStart();
        if (Autos.Count != 0)
            StartCoroutine(RunStartCountdown());
    }

    private void SetCarToStart() {
        if (StartpunktHaupt == null)
            return;

        try {
            for(int i = 0; i < Autos.Count; i++) {
                Autos[i].transform.position = StartpunktHaupt.spawnpunkte[i].position;
                Autos[i].transform.rotation = StartpunktHaupt.spawnpunkte[i].rotation;
                Autos[i].werteUndZustaende.rb.velocity = Vector3.zero;
            }
        }
        catch (MissingReferenceException) {
            Debug.LogWarning("<color=#0075cf>Es gab ein Problem mit dem Start. Probier es mal damit ihn neu ins Level zu ziehen.</color>");
        }
    }

    private IEnumerator RunStartCountdown() {
        FriereAlleAutosEin();
        EntsperreAlleKameras();
        MacheAlleAutosSteuerbar();
        
        StartpunktHaupt.SetzeFarbeAmpel(Startpunkt.Farbe.ROT);
        yield return new WaitForSecondsRealtime(2);
        StartpunktHaupt.SetzeFarbeAmpel(Startpunkt.Farbe.GELB);
        yield return new WaitForSecondsRealtime(1);
        StartpunktHaupt.SetzeFarbeAmpel(Startpunkt.Farbe.GRUEN);

        startzeit = Time.realtimeSinceStartup;
        rennenLaueft = true;
        TaueAlleAutosAuf();
    }

    private void FriereAlleAutosEin () {
        for (int i = 0; i < Autos.Count; i++) {
            Autos[i].Eingefroren = true;
        }
    }

    private void TaueAlleAutosAuf () {
        for (int i = 0; i < Autos.Count; i++) {
            Autos[i].Eingefroren = false;
        }
    }

    private void MacheAlleAutosSteuerbar () {
        for (int i = 0; i < Autos.Count; i++) {
            Autos[i].steuerbar = true;
        }
    }

    private void EntsperreAlleKameras () {
        VerfolgendeKamera[] alleVerfolgendenKameras = FindObjectsOfType<VerfolgendeKamera>();
        for (int i = 0; i < alleVerfolgendenKameras.Length; i++) {
            alleVerfolgendenKameras[i].kannSichBewegen = true;
        }
    }

    public void BeendeRennen(AutoSteuerung auto) {
        if (!rennenLaueft)
            return;
        rennenLaueft = false;

        auto.steuerbar = false;

        VerfolgendeKamera kameraZuAuto = KameraZuAuto(auto);
        if (kameraZuAuto != null)
            kameraZuAuto.kannSichBewegen = false;

        if (Oberflaeche != null) {
            Oberflaeche.MacheSichtbar();
            Oberflaeche.SetzeZeit(Time.realtimeSinceStartup - startzeit);
        }
    }

    private VerfolgendeKamera KameraZuAuto (AutoSteuerung auto) {
        VerfolgendeKamera[] alleVerfolgendenKameras = FindObjectsOfType<VerfolgendeKamera>();
        for(int i = 0; i < alleVerfolgendenKameras.Length; i++) {
            if (alleVerfolgendenKameras[i].auto == auto)
                return alleVerfolgendenKameras[i];
        }
        return null;
    }
}
