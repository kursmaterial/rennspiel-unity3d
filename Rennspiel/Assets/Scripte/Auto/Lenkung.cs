﻿using UnityEngine;

public class Lenkung : MonoBehaviour
{
    private float maxWinkel = 20f;

    private float einschlagProzent = 0;
    public float EinschlagProzent {
        private get {
            return einschlagProzent;
        }
        set {
            einschlagProzent = Mathf.Clamp(value, -1, 1);
            transform.localEulerAngles = Vector3.forward * einschlagProzent * maxWinkel;
        }
    }
}
