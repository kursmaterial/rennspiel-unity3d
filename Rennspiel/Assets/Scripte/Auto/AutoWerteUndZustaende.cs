﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[HideInInspector]
[RequireComponent(typeof(Rigidbody))]
public class AutoWerteUndZustaende : MonoBehaviour
{
    public Rigidbody rb { get; private set; }

    [HideInInspector]
    public float lenkeinschlag = 0;

    private void Awake() {
        rb = transform.GetComponent<Rigidbody>();
    }

    public bool IstAufBoden () {
        return (Physics.Raycast(transform.position + transform.up * .1f, transform.up * -1, .4f));
    }

    public float GeschwindigkeitForwaerts () {
        Vector3 projeziert = Vector3.Project(rb.velocity, transform.forward);
        int vorzeichen = (Vector3.Angle(transform.forward, rb.velocity) < 90) ? 1 : -1;
        return vorzeichen * projeziert.magnitude;
    }
}
