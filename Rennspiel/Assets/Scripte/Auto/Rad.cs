﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rad : MonoBehaviour
{
    [HideInInspector]
    public float rotationsgeschwindigkeit = 0;

    public WheelCollider wheelCollider;

    private void FixedUpdate() {
        transform.Rotate(Vector3.left * rotationsgeschwindigkeit, Space.Self);

        try {
            wheelCollider.GetWorldPose(out Vector3 colliderPosition, out Quaternion unused);
            transform.position = colliderPosition;
        } 
        catch (System.Exception ex) {
            if (ex is MissingReferenceException || ex is UnassignedReferenceException) {
                Debug.LogWarning("<color=#0075cf>Hier fehlt ein Collider. Mach den mal wieder hier hin sonst haben wir Probleme beim Kollidieren! (Das is nich so gut)</color>");
                return;
            }
            throw;
        }
    }
}
