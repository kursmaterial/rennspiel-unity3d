﻿using UnityEngine;

public class AutoKomponenten : MonoBehaviour
{
    public Rad rad_vorne_links;
    public Rad rad_vorne_rechts;
    public Rad rad_hinten_links;
    public Rad rad_hinten_rechts;

    public Lenkung lenkung_vorne_links;
    public Lenkung lenkung_vorne_rechts;
}
