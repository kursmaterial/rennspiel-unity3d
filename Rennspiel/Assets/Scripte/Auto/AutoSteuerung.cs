﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AutoWerteUndZustaende))]
[RequireComponent(typeof(AutoKomponenten))]
public class AutoSteuerung : MonoBehaviour
{
    [Serializable]
    private class Eingaben
    {
        public string vorwaerts = "w";
        public string rueckwaerts = "s";
        public string links = "a";
        public string rechts = "d";
    }

    #region Oeffentliche Attribute

    [SerializeField]
    private Eingaben eingaben = new Eingaben();

    [Range(-10, 0)]
    public float minimaleGeschwindigkeit = -5;

    [Range(0, 40)]
    public float maximaleGeschwindigkeit = 20;

    [Range(2, 20)]
    public float maximalerBeschleunigungszusatz = 12;

    [Range(0, 10)]
    public float steuerungEmpfindlichkeit = 5;

    [Range(0, 10)]
    public float beschleunigungskraft = 5;

    [Range(0, 1)]
    public float strassenhaftung = 0.1f;

    #endregion
    #region Vom Enditor geschützte Attribute

    public AutoWerteUndZustaende werteUndZustaende { get; private set; }
    public AutoKomponenten komponenten { get; private set; }

    private Vector3 geschwindigkeitVorEinfrieren = Vector3.zero;
    private bool eingefroren = false;
    public bool Eingefroren {
        get {
            return eingefroren;
        }
        set {
            if(value) {
                geschwindigkeitVorEinfrieren = werteUndZustaende.rb.velocity;
                werteUndZustaende.rb.velocity = Vector3.zero;
                werteUndZustaende.rb.constraints = RigidbodyConstraints.FreezeAll;
            } else {
                werteUndZustaende.rb.constraints = RigidbodyConstraints.None;
                werteUndZustaende.rb.velocity = geschwindigkeitVorEinfrieren;
            }
            eingefroren = value;
        }
    }

    [HideInInspector]
    public bool steuerbar = true;

    #endregion

    #region Öffentliche Methoden
    public void Bremsen(float staerke) {
        if (!werteUndZustaende.IstAufBoden())
            return;

        //Mache staerke leichter zu benutzen
        staerke /= 100;
        //Begrenze staerke zwischen 0 und 1
        if(staerke < 0) {
            Debug.LogWarning("<color=#0075cf>Du kannst nicht weniger als nicht (" + staerke + ") bremsen. So bremst das Auto einfach nicht. Dein Wert sollte größer als 0 sein.</color>");
            staerke = 0;
        }
        staerke = (1 / (-staerke - 1)) + 1;

        werteUndZustaende.rb.velocity *= (1 - staerke);
    }

    public void Beschleunigen(Vector3 richtung, float staerke) {
        if (!werteUndZustaende.IstAufBoden())
            return;

        richtung = richtung.normalized;
        if (werteUndZustaende.rb.velocity.magnitude < maximaleGeschwindigkeit + maximalerBeschleunigungszusatz) {
            float beschleunigung = Mathf.Clamp(staerke, 0, maximaleGeschwindigkeit + maximalerBeschleunigungszusatz - werteUndZustaende.rb.velocity.magnitude);
            werteUndZustaende.rb.AddForce(richtung * beschleunigung, ForceMode.VelocityChange); ;
        }
    }

    #endregion

    #region Private Methoden
    private void Awake() {
        werteUndZustaende = transform.GetComponent<AutoWerteUndZustaende>();
        komponenten = transform.GetComponent<AutoKomponenten>();
    }

    private void FixedUpdate() {
        if (steuerbar) {
            SteuereLenkung();
            SteuereBeschleunigung();
            //Optisch
            UpdateLenkeinschlag();
        }
        KompensiereRutschen();
        //Optisch
        UpdateRadgeschwindigkeit();
        
    }

    private void SteuereBeschleunigung () {
        if(Input.GetKey(eingaben.vorwaerts)) {          
            BeschleunigeBegrenztVorwaerts();
        }
        else if(Input.GetKey(eingaben.rueckwaerts)) {
            BeschleunigeBegrenztRueckwaerts();
        }
    }

    private void SteuereLenkung () {
        if(Input.GetKey(eingaben.rechts)) {
            Lenke(1);
            werteUndZustaende.lenkeinschlag = 1;
        }
        else if(Input.GetKey(eingaben.links)) {
            Lenke(-1);
            werteUndZustaende.lenkeinschlag = -1;
        }
        else {
            werteUndZustaende.lenkeinschlag = 0;
        }
    }

    private void Lenke (float einschlagProzent) {
        if(werteUndZustaende.IstAufBoden()) {
            einschlagProzent = Mathf.Clamp(einschlagProzent, -1f, 1f);

            float einschlag = werteUndZustaende.GeschwindigkeitForwaerts() / maximaleGeschwindigkeit;
            einschlag *= steuerungEmpfindlichkeit;
            einschlag *= einschlagProzent;

            Quaternion neueRotation = transform.rotation * Quaternion.Euler(0, einschlag, 0);

            werteUndZustaende.rb.MoveRotation(neueRotation);
        }
    }

    private void BeschleunigeBegrenztVorwaerts () {
        if (werteUndZustaende.GeschwindigkeitForwaerts() < maximaleGeschwindigkeit) {
            if (werteUndZustaende.IstAufBoden()) {
                float beschleunigung = Mathf.Clamp(beschleunigungskraft * .1f, 0, maximaleGeschwindigkeit - werteUndZustaende.GeschwindigkeitForwaerts());
                werteUndZustaende.rb.AddForce(transform.forward * beschleunigung, ForceMode.VelocityChange); ;
            }
        }
    }
    
    private void BeschleunigeBegrenztRueckwaerts() {
        if (werteUndZustaende.GeschwindigkeitForwaerts() > minimaleGeschwindigkeit) {
            if (werteUndZustaende.IstAufBoden()) {
                float beschleunigung = Mathf.Clamp(beschleunigungskraft * -.03f, minimaleGeschwindigkeit - werteUndZustaende.GeschwindigkeitForwaerts(), 0);
                werteUndZustaende.rb.AddForce(transform.forward * beschleunigung, ForceMode.VelocityChange);
            }
        }
    }

    private void UpdateRadgeschwindigkeit () {
        UpdateRadgeschwindigkeitVorne();
        UpdateRadgeschwindigkeitHinten();
    }

    private void UpdateRadgeschwindigkeitVorne () {
        
        if(werteUndZustaende.IstAufBoden()) {
            float rotationsgeschwindigkeit = werteUndZustaende.GeschwindigkeitForwaerts();
            try {
                komponenten.rad_vorne_links.rotationsgeschwindigkeit = rotationsgeschwindigkeit;
                komponenten.rad_vorne_rechts.rotationsgeschwindigkeit = rotationsgeschwindigkeit;
            }
            catch (NullReferenceException) {
                Debug.LogWarning("<color=#0075cf>Mach das Vorderrad da wieder dran!</color>");
            }
        }
        
}

    private void UpdateRadgeschwindigkeitHinten () {
        try {
            if (werteUndZustaende.IstAufBoden()) {
                SetzeRadgeschwindigkeitHintenAufBoden();
            }
            else {
                SetzeRadgeschwindigkeitHintenInLuft();
            }
        }
        catch (NullReferenceException) {
            Debug.LogWarning("<color=#0075cf>Mach das Hinterrad da wieder dran!</color>");
        }
    }

    private void SetzeRadgeschwindigkeitHintenAufBoden () {
        float geschwindigkeitVorwaerts = werteUndZustaende.GeschwindigkeitForwaerts();
        int vorzeichen = (geschwindigkeitVorwaerts >= 0) ? 1 : -1;
        float faktor = 1;
        if (Input.GetKey(eingaben.vorwaerts) && geschwindigkeitVorwaerts < maximaleGeschwindigkeit) {
            faktor = (geschwindigkeitVorwaerts >= 0) ? 1.4f : 0.6f;
        }
        else if (Input.GetKey(eingaben.rueckwaerts) && geschwindigkeitVorwaerts > minimaleGeschwindigkeit) {
            faktor = (geschwindigkeitVorwaerts >= 0) ? 1.2f : 0.8f;
        }
        komponenten.rad_hinten_links.rotationsgeschwindigkeit = geschwindigkeitVorwaerts * faktor;
        komponenten.rad_hinten_rechts.rotationsgeschwindigkeit = geschwindigkeitVorwaerts * faktor;
    }

    private void SetzeRadgeschwindigkeitHintenInLuft () {
        float faktor = 1;
        if (Input.GetKey(eingaben.vorwaerts)) {
            faktor = 2f;
        } 
        else if (Input.GetKey(eingaben.rueckwaerts)) {
            faktor = -1.5f;
        }
        komponenten.rad_hinten_links.rotationsgeschwindigkeit = 30 * faktor;
        komponenten.rad_hinten_rechts.rotationsgeschwindigkeit = 30 * faktor;
    }

    private void UpdateLenkeinschlag () {
        int einschlag = 0;
        if (Input.GetKey(eingaben.links))
            einschlag = -1;
        if (Input.GetKey(eingaben.rechts))
            einschlag = 1;
        komponenten.lenkung_vorne_links.EinschlagProzent = einschlag;
        komponenten.lenkung_vorne_rechts.EinschlagProzent = einschlag;
    }

    private void KompensiereRutschen () {
        if (!werteUndZustaende.IstAufBoden())
            return;

        Vector3 istRichtung = werteUndZustaende.rb.velocity;
        Vector3 sollRichtung = Vector3.Project(istRichtung, transform.forward);

        //erleichterter Input, genauere einstellung möglich durch Quadrieren. Außerdem Absicherung gegen Werte > 1
        float echteStrassenhaftung = Mathf.Clamp01(strassenhaftung * strassenhaftung);

        Vector3 neueRichtung = istRichtung + (sollRichtung - istRichtung) * echteStrassenhaftung;
        werteUndZustaende.rb.velocity = neueRichtung;
    }
    #endregion
}
