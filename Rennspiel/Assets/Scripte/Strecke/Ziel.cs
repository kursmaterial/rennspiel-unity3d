﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Ziel : MonoBehaviour
{
    private void OnTriggerEnter(Collider other) {
        if (other.transform.GetComponent<AutoSteuerung>()) {
            Spielmanager.Instanz.BeendeRennen(other.transform.GetComponent<AutoSteuerung>());
        }
    }
}
