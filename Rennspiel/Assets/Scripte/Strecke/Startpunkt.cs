﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Startpunkt : MonoBehaviour
{
    public Transform[] spawnpunkte = new Transform[4];
    public GameObject[] ampeln;

    public Material gruenesMaterial;
    public Material gelbesMaterial;
    public Material rotesMaterial;

    public Light[] lampen;

    public Color grueneLampenfarbe;
    public Color gelbeLampenfarbe;
    public Color roteLampenfarbe;

    public enum Farbe { GRUEN, GELB, ROT}

    public void SetzeFarbeAmpel (Farbe farbe) {
        switch(farbe) {
            case Farbe.GRUEN: SetzteFarbeGruen(); break;
            case Farbe.GELB: SetzteFarbeGelb(); break;
            case Farbe.ROT: SetzteFarbeRot(); break;
            default: SetzteFarbeRot(); break;
        }
    }

    private void SetzteFarbeGruen() {
        for(int i = 0; i < ampeln.Length; i++) {
            ampeln[i].GetComponent<Renderer>().material = gruenesMaterial;
        }
        for(int i = 0; i < lampen.Length; i++) {
            lampen[i].color = grueneLampenfarbe;
        }
    }

    private void SetzteFarbeGelb() {
        for (int i = 0; i < ampeln.Length; i++) {
            ampeln[i].GetComponent<Renderer>().material = gelbesMaterial;
        }
        for (int i = 0; i < lampen.Length; i++) {
            lampen[i].color = gelbeLampenfarbe;
        }
    }

    private void SetzteFarbeRot() {
        for (int i = 0; i < ampeln.Length; i++) {
            ampeln[i].GetComponent<Renderer>().material = rotesMaterial;
        }
        for (int i = 0; i < lampen.Length; i++) {
            lampen[i].color = roteLampenfarbe;
        }
    }
}
