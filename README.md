# Modulares Rennspiel in Unity3D


## Über das Projekt

Dieses Unity Projekt dient der Erklärung und Lehre der Unity Engine für
Anfänger. Das Projekt ist so strukturiert, dass die folgenden Grundlagen
vermittelt werden können:
- Überblick über die wichtigsten Elemente des Unity Editors (Hierarchy,
  Scene, Game, Insprector und Project)
- Einfügen, positionieren, rotieren (und skalieren) von 3D-Objekten und
  vor allem Prefabs in einer 3D Szene
- Eine Übersicht davon, was Components sind und wie diese im Inspektor
  angezeigt werden und manipuliert werden können
- Hinzufügen eigener Klassen und die Programmierung rudimentärer Funktionen

Darüber hinaus kann das Projekt nahezu beliebig erweitert oder gekürzt
werden indem entweder Inhalte übersprungen werden oder tiefer in die
Erklärung und Erweiterung des bestehenden Codes eingestiegen wird.

Das Projekt befindet sich mit der im Projektverzeichnis angegebenen
"Unlicense" Lizenz in der öffentlichen Domäne und kann somit nach
freiem Belieben verändert werden.


## Systemanforderungen / Entwicklungsumgebung

Das Projekt wird auf dem folgenden System entwickelt und gestestet:

| Software | Version |
| --- | --- |
| Betriebssystem | Windows 10 |
| Unity | Unity 2019.4.31f1 LTS |

Theoretisch ist es auch möglich das Projekt auf MacOS oder Linux
oder mit einer neueren Unity Version auszuführen, dies ist jedoch nicht
getestet. Demnach ist es empfehlenswert das angegebene Setup zu verwenden.

**Entwickler** die an dem Projekt arbeiten wollen müssen die selbe Unity
Version benutzen.


## Verteilung an Kursteilnehmer

Der Empfohlene Weg das Projekt an Teilnehmer zu verteilen ist es, nicht das
gesamte Repository zu verschicken sondern lediglich den "Rennspiel" Ordner.
Die Empfehlung kommt daher, dass Dateien wie z.B. diese Readme oder
gewisse Kursunterlagen die Teilnehmer nicht betreffen und somit zu längern
Downloadzeiten führen und potentiell Verwirrung bei Teilnehmern stiften
können.

Im Optimalfall wird der "Rennspiel" Ordner komprimiert und den Teilnehmern
über einen Filesharing Dienst zur Verfügung gestellt.


## Für Entwickler

Das Projekt sollte nach jedem Push auf `main` stets in einem Zustand sein,
in welchem es Teilnehmern übergeben werden kann. Sollte es nötig sein eigen
Dateien, wie z.B. Testszenen anzulegen sollte dazu der Ordner
`/Rennspiel/Assets/Tests` angelegt werden. Der Ordner und seine Inhalte
werden mittel der `.gitingore` ignoriert, von Git nicht in die
Versionskontrolle aufgenommen und somit auch nicht gepusht.